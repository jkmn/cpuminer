package main

import (
	"github.com/jackmanlabs/bucket/jlog"
	"github.com/jackmanlabs/cpuminer/stratum"
	"github.com/jackmanlabs/errors"
	"log"
	"runtime"
	"time"
)

func main() {

	qtyCpus := runtime.NumCPU()
	log.Print("CPU Quantity: ", qtyCpus)

	uri := "stratum+tcp://scrypt.usa.nicehash.com:3333/#xnsub"

	conn, err := stratum.Connect(uri)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	// Nothing should be coming in until we subscribe and authenticate, but I
	// prefer to have channels ready.
	jobs := conn.WaitJobs()

	conn.Subscribe()

	user := "1Be9zoXrtyrpSsLyEvy37uy9ENyXA92Uty"
	pass := "x"
	conn.Authenticate(user, pass)

	timeout := time.After(5 * time.Second)

	for {
		select {
		case job := <-jobs:
			jlog.Log(job)
		case <-timeout:
			return
		}
	}

}

//func processJobs(jobs chan stratum.Job, workers int){
//
//	killswitch := make(chan bool)
//
//	for job := range jobs{
//
//		if job.CleanJobs{
//			close (killswitch)
//			killswitch = make(chan bool)
//		}
//
//		for offset := 0; offset < workers; offset++{
//			processJob(job, offset, killswitch)
//		}
//	}
//
//}
//
//func processJob(job stratum.Job, offset int, killSwitch chan bool){
//	var hasher hash.Hash = sha256.New()
//
//
//
//	var coinbase  string := job.CoinB1 + job.Extranonce1 + job.Extranonce2 + job.CoinB2
//	coinbaseHashBin := hasher.Sum()
//}
