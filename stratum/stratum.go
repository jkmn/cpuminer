package stratum

import (
	"encoding/json"
	"fmt"
	"github.com/jackmanlabs/bucket/jlog"
	"github.com/jackmanlabs/errors"
	"io"
	"log"
	"math"
	"net"
	"net/url"
	"os"
	"sync"
)

const (
	BASE_TARGET float64 = 0x00000000FFFF0000000000000000000000000000000000000000000000000000
)

type Conn struct {
	net.Conn

	killSwitch chan bool
	msgsIn     chan IncommingMessage
	msgsOut    chan OutgoingMessage
	jobsIn     chan Job

	// Mutex for state variables:
	stateMutex sync.Mutex

	// State variables:
	subscriptionIds map[string]string // map[type]id
	authenticated   bool
	idCounter       int
	extranonce1     string
	extranonce2Size uint
	extranonce2     uint
	target          float64

	pendingResponse map[int]OutgoingMessage
}

type OutgoingMessage struct {
	Id         int           `json:"id"`
	Method     string        `json:"method"`
	Parameters []interface{} `json:"params"`
}

type IncommingMessage struct {
	Id         *int            `json:"id,omitempty"`
	Method     *string         `json:"method,omitempty"`
	Result     json.RawMessage `json:"result,omitempty"`
	Parameters []interface{}   `json:"params,omitempty"`
	Error      interface{}     `json:"error,omitempty"`
}

type Job struct {

	// These are issued by the server.
	JobId          string
	PreviousHash   string
	CoinB1         string
	CoinB2         string
	MerkleBranches []string
	Version        string
	NBits          string
	NTime          string
	CleanJobs      bool

	// These are for convenience in individual job processing.
	Extranonce1 string
	Extranonce2 string
	Target      float64
}

func (this *Job) Coinbase() string {
	return this.CoinB1 + this.Extranonce1 + this.Extranonce2 + this.CoinB2
}

func Connect(uri string) (*Conn, error) {

	u, err := url.Parse(uri)
	if err != nil {
		return nil, errors.Stack(err)
	}

	conn, err := net.Dial("tcp", u.Host)
	if err != nil {
		return nil, errors.Stack(err)
	}

	this := &Conn{
		Conn:            conn,
		killSwitch:      make(chan bool),
		msgsIn:          make(chan IncommingMessage),
		msgsOut:         make(chan OutgoingMessage),
		pendingResponse: make(map[int]OutgoingMessage),
		subscriptionIds: make(map[string]string),
	}

	go this.listen()
	go this.transmit()
	go this.handleMessages()

	return this, nil
}

/*
Authenticates (registers) user data for the connection. For NiceHash, use your
BitCoin key and "x".
*/
func (this Conn) Authenticate(user, pass string) {
	req := OutgoingMessage{
		Method:     "mining.authorize",
		Parameters: []interface{}{user, pass},
	}

	this.msgsOut <- req
}

/*
This method should be the first called. It indicates to the server that you are
listening to incoming messages. Some servers (such as NiceHash) will disconnect
immediately if this is not the first message.
*/
func (this Conn) Subscribe() {
	req := OutgoingMessage{
		Method:     "mining.subscribe",
		Parameters: []interface{}{},
	}

	this.msgsOut <- req
}

func (this Conn) listen() {

	var err error

	for {
		var msg IncommingMessage
		err = json.NewDecoder(this).Decode(&msg)
		if err != nil {
			log.Print(errors.Stack(err))
			close(this.killSwitch)
			return
		}

		select {
		case this.msgsIn <- msg:
		case <-this.killSwitch:
			return
		}
	}

}

func (this Conn) transmit() {

	for {
		select {
		case request := <-this.msgsOut:

			this.stateMutex.Lock()
			request.Id = this.idCounter
			this.idCounter++
			this.pendingResponse[request.Id] = request
			this.stateMutex.Unlock()

			err := json.NewEncoder(io.MultiWriter(os.Stderr, this)).Encode(request)
			if err != nil {
				log.Print(errors.Stack(err))
				close(this.killSwitch)
				return
			}

		case <-this.killSwitch:
			return
		}
	}
}

func (this Conn) handleMessages() {

	var err error

LOOP:
	for {
		select {
		case msg := <-this.msgsIn:

			if msg.Method != nil {

				// This message is a notification.

				switch *msg.Method {
				case "mining.set_difficulty":

					difficulty := msg.Parameters[0].(float64)

					this.stateMutex.Lock()
					this.target = BASE_TARGET / difficulty
					log.Printf("New difficulty: %f", difficulty)
					log.Printf("New target: %x", math.Float64bits(this.target))
					this.stateMutex.Unlock()

				case "mining.notify":

					merkleBranches := make([]string, 0)
					merkleBranches_ := msg.Parameters[4].([]interface{})
					for _, merkleBranch_ := range merkleBranches_ {
						merkleBranches = append(merkleBranches, merkleBranch_.(string))
					}

					this.stateMutex.Lock()

					extranonce2 := fmt.Sprintf("%08x", this.extranonce2)

					job := Job{
						JobId:          msg.Parameters[0].(string),
						PreviousHash:   msg.Parameters[1].(string),
						CoinB1:         msg.Parameters[2].(string),
						CoinB2:         msg.Parameters[3].(string),
						MerkleBranches: merkleBranches,
						Version:        msg.Parameters[5].(string),
						NBits:          msg.Parameters[6].(string),
						NTime:          msg.Parameters[7].(string),
						CleanJobs:      msg.Parameters[8].(bool),
						Extranonce1:    this.extranonce1,
						Extranonce2:    extranonce2,
						Target:         this.target,
					}

					this.extranonce2 = nextExtranonce2(this.extranonce2, this.extranonce2Size)

					this.stateMutex.Unlock()

					this.jobsIn <- job
				default:
					log.Print("Unhandled notification:")
					jlog.Log(msg)
				}
			} else if msg.Id != nil {

				// This message is a response.

				this.stateMutex.Lock()
				request := this.pendingResponse[*msg.Id]
				delete(this.pendingResponse, *msg.Id)
				this.stateMutex.Unlock()

				log.Print("Handling response: ", request.Method)

				switch request.Method {

				case "mining.subscribe":

					results := make([]interface{}, 0)
					err = json.Unmarshal(msg.Result, &results)
					if err != nil {
						err = errors.Stack(err)
						break LOOP
					}

					log.Print("Results:")
					jlog.Log(results)

					subscriptions := results[0].([]interface{})
					extranonce1 := results[1].(string)
					extranonce2Size := results[2].(float64)

					this.stateMutex.Lock()

					var (
						subscriptionName string
						subscriptionId   string
					)

					for k, subscription_ := range subscriptions {
						if k%2 == 0 {
							subscriptionName = subscription_.(string)
						} else {
							subscriptionId = subscription_.(string)
							this.subscriptionIds[subscriptionName] = subscriptionId
							log.Printf("Subscribed: %s (%s)", subscriptionName, subscriptionId)
						}
					}

					this.extranonce1 = extranonce1
					this.extranonce2Size = uint(extranonce2Size)

					log.Printf("New Extranonce1: %s", extranonce1)
					log.Printf("New Extranonce2 Size: %f", extranonce2Size)

					this.stateMutex.Unlock()

				case "mining.authorize":

					var result bool
					err = json.Unmarshal(msg.Result, &result)
					if err != nil {
						err = errors.Stack(err)
						break LOOP
					}

					this.stateMutex.Lock()
					this.authenticated = result
					this.stateMutex.Unlock()

				default:
					log.Print("Unhandled response:")
					jlog.Log(msg)
				}

			} else {
				log.Print("Unknown message type received:")
				jlog.Log(msg)
			}

		case <-this.killSwitch:
			return
		}

		if err != nil {
			log.Print(errors.Stack(err))
			close(this.killSwitch)
			return
		}
	}
}

/*
This returns a channel of jobs, which are put on the channel as they are
received. The same channel is returned with each call; the caller is responsible
for dealing with that channel responsibly.
*/
func (this Conn) WaitJobs() <-chan Job {
	return this.jobsIn
}

func nextExtranonce2(current uint, size uint) uint {

	var max uint = (1 << size) - 1

	current++
	if current > max {
		current = 0
	}

	return current
}
