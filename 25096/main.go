package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"github.com/jackmanlabs/cpuminer/stratum"
	"github.com/jackmanlabs/errors"
	"hash"
	"log"
	"math"
)

const BASE_TARGET float64 = 0x00000000FFFF0000000000000000000000000000000000000000000000000000

func main() {
	//job := stratum.Job{
	//	JobId:          "bf",
	//	PreviousHash:   "4d16b6f85af6e2198f44ae2a6de67f78487ae5611b77c6c0440b921e00000000",
	//	CoinB1:         "01000000010000000000000000000000000000000000000000000000000000000000000000ffffffff20020862062f503253482f04b8864e5008",
	//	CoinB2:         "072f736c7573682f000000000100f2052a010000001976a914d23fcdf86f7e756a64a7a9688ef9903327048ed988ac00000000",
	//	MerkleBranches: []string{},
	//	Version:        "00000002",
	//	NBits:          "1c2ac4af",
	//	NTime:          "504e86b9",
	//	CleanJobs:      false,
	//
	//	Extranonce1: "08000002",
	//	Extranonce2: "00000001",
	//}

	var difficulty float64 = 512

	job := stratum.Job{
		JobId:          "5c04",
		PreviousHash:   "da0dadb0eda4381df442bde08d23d54d7d371d5ce7af3ee716bd2a7e017eacb8",
		CoinB1:         "01000000010000000000000000000000000000000000000000000000000000000000000000ffffffff2a03700a08062f503253482f04953f1a5308",
		CoinB2:         "102f776166666c65706f6f6c2e636f6d2f0000000001d07e582a010000001976a9145d8f33b0a7c94c878d572c40cbff22a49268467d88ac00000000",
		MerkleBranches: []string{"50a4a386ab344d40d29a833b6e40ea27dab6e5a79a2f8648d3bc0d1aa65ecd3f", "7952ecc836fb104f41b2cb06608eeeaa6d1ca2fe4391708fb13bb10ccf8da179", "9400ec6453aac577fb6807f11219b4243a3e50ca6d1c727e6d05663211960c94", "c11a630fa9332ab51d886a47509b5cbace844316f4fc52b493359b305fd489ae", "85891e7c5773f234d647f1d5fca7fbcabb59b261322d16c0ae486ccf5143383d", "faa26bbc17f99659f64136bea29b3fc8d772b339c52707d5f2ccfe1195317f43"},
		Version:        "00000002",
		NBits:          "1b10b60e",
		NTime:          "531a3f95",
		CleanJobs:      false,

		Extranonce1: "60021014",
		Extranonce2: "00000000",
		Target:      BASE_TARGET / difficulty,
	}

	// 0xFFFFFFFF is the capacity of a 32-bit integer.
	//for nonce :=0; nonce <= 0xFFFFFFFF ; nonce++{
	//
	//}

	coinbase_hex := job.CoinB1 + job.Extranonce1 + job.Extranonce2 + job.CoinB2

	coinbase_bin, err := hex.DecodeString(coinbase_hex)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	var coinbaseBinHash []byte
	var hasher hash.Hash = sha256.New()

	hasher.Write(coinbase_bin)
	coinbaseBinHash = hasher.Sum(nil)
	hasher.Reset()
	hasher.Write(coinbaseBinHash)
	coinbaseBinHash = hasher.Sum(nil)

	log.Printf("coinbaseBinHash:\t%x", coinbaseBinHash)

	merkleRootBin, err := buildMerkleRoot(job.MerkleBranches, coinbaseBinHash)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	log.Printf("merkleRootBin:\t%x", merkleRootBin)

	merkeRootHex := hex.EncodeToString(merkleRootBin)

	nonce := 1
	nonceHex := fmt.Sprintf("%08x", nonce)

	log.Print("Version:\t\t", job.Version)
	log.Print("Previous Hash:\t", job.PreviousHash)
	log.Print("Merkle Root:\t", merkeRootHex)
	log.Print("Time:\t\t\t", job.NTime)
	log.Print("Bits:\t\t\t", job.NBits)
	log.Print("Nonce:\t\t\t", nonceHex)
	log.Printf("Target:\t\t\t%x", math.Float64bits( job.Target))

	padding := "000000800000000000000000000000000000000000000000000000000000000000000000000000000000000080020000"
	log.Print(padding)
	padding = reverse(padding)
	log.Print(padding)

	block_header := job.Version + job.PreviousHash + merkeRootHex + job.NTime + job.NBits + nonceHex + padding

	log.Print(block_header)
}

func buildMerkleRoot(merkleBranch []string, coinbase_hash_bin []byte) ([]byte, error) {
	merkleRoot := coinbase_hash_bin
	for _, merkleBranchHex := range merkleBranch {
		merkleBranchBin, err := hex.DecodeString(merkleBranchHex)
		if err != nil {
			return nil, errors.Stack(err)
		}

		merkleRoot = append(merkleRoot, merkleBranchBin...)

		var hasher hash.Hash = sha256.New()

		hasher.Write(merkleRoot)
		merkleRoot = hasher.Sum(nil)
		hasher.Reset()
		hasher.Write(merkleRoot)
		merkleRoot = hasher.Sum(nil)
	}

	return merkleRoot, nil
}

func reverse(hx string) string {

	data, _ := hex.DecodeString(hx)
	buf := bytes.NewBuffer(data)

	ints := make([]uint32, len(data)/4)

	err := binary.Read(buf, binary.BigEndian, ints)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	buf.Reset()
	err = binary.Write(buf, binary.LittleEndian, ints)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	return hex.EncodeToString(buf.Bytes())
}
